%global pkg_name %{name}

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           preupgrade-assistant-el6toel7
Version:        0.8.0
Release:        3%{?dist}
Summary:        Set of modules created for upgrade to Red Hat Enterprise Linux 7
Group:          System Environment/Libraries
License:        GPLv3+
URL:            https://github.com/upgrades-migrations/preupgrade-assistant-modules
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{pkg_name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       preupgrade-assistant >= 2.5.0
BuildRequires:  preupgrade-assistant-tools >= 2.5.0

# static data are required by our modules & old contents
# are obsoleted by this package
Obsoletes:      preupgrade-assistant-contents < 0.6.41-6
Provides:       preupgrade-assistant-contents = %{version}-%{release}
Requires:       preupgrade-assistant-el6toel7-data


############################
# Per module requirements #
############################

# * owned by RHEL6_7/packages/pkgdowngrades - this is for the
#   redhat-upgrade-tool hook "fixpkgdowngrades.sh
Requires:       yum, rpm-python

# Why those requirements?
# /usr/bin/python and /usr/bin/python2 are already present.
Requires:       bash
Requires:       python
Requires:       coreutils
Requires:       perl

#do not require php even in presence of php scripts
#dependency filtering: https://fedoraproject.org/wiki/Packaging:AutoProvidesAndRequiresFiltering
%if 0%{?fedora} || 0%{?rhel} > 6
%global __requires_exclude_from ^%{_datadir}
%else
%filter_requires_in %{_datadir}
%filter_setup
%endif

##############################
#  PATCHES HERE
##############################
Patch0001:  0001-UEFI-fix-1-2-Fix-the-broken-bootloader-when-upgrade-.patch
Patch0002:  0002-UEFI-fix-2-2-Fix-the-broken-bootloader-when-upgrade-.patch

####### PATCHES END ##########

# We do not want any autogenerated requires/provides based on content
%global __requires_exclude .*
%global __requires_exclude_from .*
%global __provides_exclude .*
%global __provides_exclude_from .*

%description
The package provides a set of modules used for assessment
of the source system for upgrade or migration to Red Hat
Enterprise Linux 7 system.
The modules are used by the preupgrade-assistant package.


%prep
%setup -q -n %{name}-%{version}
%patch0001 -p1
%patch0002 -p1

### REMOVE .orig files
# I don't know why, but one .orig file is permanently created...
find . -name "*.orig" -delete

%build
# This is all we need here. The RHEL6_7-results dir will be created
# with XCCDF files for Preupgrade Assistant and OpenSCAP
preupg-xccdf-compose RHEL6_7


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p -m 755 $RPM_BUILD_ROOT%{_datadir}/doc/preupgrade-assistant-el6toel7
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/preupgrade
mv LICENSE $RPM_BUILD_ROOT%{_datadir}/doc/preupgrade-assistant-el6toel7/
mv RHEL6_7-results $RPM_BUILD_ROOT%{_datadir}/preupgrade/RHEL6_7

rm -rf $RPM_BUILD_ROOT/%{_datadir}/preupgrade/common

# General cleanup
find $RPM_BUILD_ROOT%{_datadir}/preupgrade/RHEL6_7 -regex ".*/\(module\|group\)\.ini$" -regextype grep -delete
find $RPM_BUILD_ROOT%{_datadir}/preupgrade/ -name "READY" -delete
find $RPM_BUILD_ROOT -name '.gitignore' -delete
find $RPM_BUILD_ROOT -name 'module_spec' -delete


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%dir %{_datadir}/doc/preupgrade-assistant-el6toel7/
%doc %{_datadir}/doc/preupgrade-assistant-el6toel7/LICENSE
%dir %{_datadir}/preupgrade/RHEL6_7/
%{_datadir}/preupgrade/RHEL6_7/



%changelog
* Thu Apr 08 2021 Petr Stodulka <pstodulk@redhat.com> - 0.8.0-3
- Fix broken bootloader on systems with UEFI when upgrade is run without
  the --cleanup-post option
- Resolves: rhbz#1915393

* Tue Jul 21 2020 Petr Stodulka <pstodulk@redhat.com> - 0.8.0-2
- Drop the TargetEUFInfo modul - it's obsoleted because of RHEL 7.9
- Related: rhbz#1859309

* Tue Jul 21 2020 Petr Stodulka <pstodulk@redhat.com> - 0.8.0-1
- Bump to version 0.8.0
- Switch the upgrade path to RHEL 6.10 -> RHEL 7.9
- Enable upgrades with EFI boot
- Inhibit upgrade when by-path is used in /etc/fstab
- Drop the broken InterVariants module
- Fix and improve migration of tuned
- Fix networking issues around network-manager and route-eth* files
- Fix report from the networking/openldap module
- Fix syntax error in the other/configchanges module
- Handle dead symlinks in the system/SysconfigCgroupDaemon module
- Resolves: rhbz#1859309

* Tue Jul 09 2019 Petr Stodulka <pstodulk@redhat.com> - 0.7.9-3
- Make the newly created module executable explicitly as the build server
  ignores chmod in the patch file
  Related: rhbz#1723937

* Wed Jun 26 2019 Michal Bocek <mbocek@redhat.com> - 0.7.9-2
- Inform about consequences of upgrading to nonlatest RHEL 7 minor version
  Related: rhbz#1723937

* Fri Feb 15 2019 Petr Stodulka <pstodulk@redhat.com> - 0.7.9-1
- Remove legacy grub before installation of grub2
  Related: rhbz#1618926

* Tue Feb 12 2019 Petr Stodulka <pstodulk@redhat.com> - 0.7.8-1
- Add check for Linux Disk Layout usage on s390x arch
  Resolves: rhbz#1618926

* Fri Oct 05 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.7-1
- services/tomcat: migrate config files and webapps to the new paths correctly
- networking/samba: improve the informational message
- Related: rhbz#1613347

* Wed Sep 05 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.6-1
- system/pam: fixes hanging in specific cases and improves report
- system/BinariesRebuild: fixes issues with empty blacklist file
- backup configuration files only into dirtyconf and cleanconf dirs
- add module to inhibit upgrade when system/java-1.8.0-ibm is installed
- fixes chmods when it is handled in python scripts
  Resolves: rhbz#1613347


* Tue Jun 12 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.5-1
- system/requirements: check dmidecode and dmesg before use
- set correctly custom selinux policy set by semanage
- fix the initscripts module and follow the best practices
  Related: rhbz#1585208

* Fri Jun 01 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.4-1
- set upgrade path RHEL 6.10 -> RHEL 7.6
- rename correctly netowrk interfaces regardless amount of existing
  interfaces
- system/java: handle corerectly i686 RPMs
- system/grubby: set as not applicable on ppc64 arch
  Resolves: rhbz#1585208

* Wed Apr 04 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.3-1
- Unset the release when it is set in subscription-manager
- add quotes for readlink in drivers/* modules so they will not crash
- In case of multiple network interfaces with the "ethX" naming, their names
  are changed to a persistent custom "netX" to retain the functioning network
  across the upgrade
- openssh/sshd: the module has been rewritten significantly to
  provide correct data to user and be sure the sshd configuration is valid
- Related: rhbz#1546107

* Fri Feb 16 2018 Petr Stodulka <pstodulk@redhat.com> - 0.7.2-1
- check free space on boot
- system/requirements: fix check of RAM size
- Provide directory for third party modules.
- initscripts/control: update preset file for RHEL7.5
- system/python: handle ownership of directories by more rpms
- Resolves: rhbz#1546107

* Thu Nov 09 2017 Jakub Mazanek <jmazanek@redhat.com> - 0.7.1-1
- text polishing
- initscripts/control: Add msg how to migrate custom init scripts
- to unit files
- removed stray output from few modules
- networking/dovecot: resolves troubles with first_valid_uid
- rewrite openssh/sysconfig to provide correct output data and fix
- false negative report
  Resolves: rhbz#1372872 rhbz#1388967

* Sun Oct 22 2017 Petr Stodulka <pstodulk@redhat.com> - 0.7.0-2
- remove patch leftovers

* Sun Oct 22 2017 Petr Stodulka <pstodulk@redhat.com> - 0.7.0-1
- added new informational module for the Samba
- ensure the grub2 rpm is installed after the upgrade to new system
  Resolves: #1497731
- modules are compatible with Preupgrade Assistant v2.5.0
  - added script that initialize part of environment for modules
  - added the properties.ini file with additional metadata about modules
  - modified modules to not use deprecated funcionality
- modified openssh/sysconfig module to remove astray output and provide
  cleaner information with relevant risk level
- text polishing
- removed pointless dependency on the redhat-upgrade-tool
  Resolves: #1497731 #1503757 #1402481

* Thu Jul 13 2017 Jakub Mazanek <jmazanek@redhat.com> - 0.6.71-1
- mysql: fixing harmful typos
  Resolves: #1470747

* Wed Jun 28 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.70-2
- yaboot: create directory structure for grub file
  Related: #1296658

* Fri Jun 23 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.70-1
- forbid in-place upgrade when UEFI is used
  Resolves: #1462672

* Thu Jun 22 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.69-2
- added check of available space on the /boot partition
  Resolves: #1276577

* Wed Jun 21 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.69-1
- bind-chroot: backup only relevant config files
- use correct risk level for downgraded packages
- modified yaboot module to print correct info and do not create
  the /etc/default/grub.conf file automatically, but keep it on user
  to be sure that content of the file is really valid
- text polishing
- grub: added instructions how to migrate GRUB to GRUB2
- Resolves: #1416324 #1414873 #1296658 #1447705 #1447706 #1458956

* Tue May 30 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.68-1
- fix ownership of the /usr/share/preupgrade/RHEL6_7 directory
- text polishing
- initscripts: enable correctly services according to preset file
- SonameBump: fix solution text
- java: fix postupgrade-script so there is not more error message
- check correctly if the /usr is located on the root partition
- bind: fix hang up for specific configuration files
- bind: fixes the options statement correctly when it is located inside
  of included files and do not create duplicit occurrence of the 'pid-file'
  statement when it is already located
- fixes the install_rpmlist.sh script and improve performance of installation
  of packages inside lists prepared by the modules
- backup selinux configuration file
- Resolves: #1414402, #1414556, #1452582, #1456193, #1456196, #1449231,
  -         #1456628, #1456630, #1447705, #1447706

* Wed May 03 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.67-1
- text polishing
- rsyslog: use default configuration file on target system
- dovecot: check and fix the first_valid_uid value
- Resolves: #1447706, #1447705, #1387159, #1388967

* Thu Feb 28 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.66-3
- text polishing
  Resolves: #1393429

* Thu Feb 02 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.66-2
- system/BinariesRebuild performance workround
- Enable use of blacklist, which filters all executable files that
  are on path, which begins with prefix matched in the blacklist.
  For activation append these lines (without whitespace at beginning of lines)
  into the /etc/preupgrade-assistant.conf:
  [xccdf_preupg_rule_system_BinariesRebuild_check]
  exclude_file=<full-path-to-your-blacklist>
  Resolves: #1392018

* Mon Jan 23 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6.66-1
- java: fix call of log_* functions and init files
  Resolves: #1293213
- make modules compatible with Preupgrade Assistant v2.3.0
  - remove use of component provided by PA API
  Resolves: #1405649
- usrmgmt/ReserverIDs - text polishing
  Resolves: #1393429

* Tue Jan 10 2017 Petr Stodulka <pstodulk@redhat.com> - 0.6-65-2
- fix wrong format of jvmdir for Java OpenJDK 8
  Resolves: #1411439

* Tue Dec 20 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.65-1
- added modules: system/java and networking/RHDS
- removed the packages/java-provide module obsoleted by java above
- polishing of texts
- unified names of key files of modules
- Resolves: #1293213, #1393426, #1393429, #1395397, #1402478, #1406464

* Wed Dec 07 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.60-1
- added the tomcat module
- polishing of texts
- updated for RHEL 6.9 system
- update modules to use current PA API
- ha-cluster module now requires action instead of simply fail
- Resolves: #1281335, #1393475, #1393081, #1375185, #1402052

* Fri Oct 21 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.59-5
- networking/openldap
- adjusts exit_status to get needs_inspection/needs_action as needed
- shortens the log message
  Resolves: #1358662

* Tue Oct 18 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.59-4
- revert #1356808 because currently there is not provided suitable
  place to store such file

* Wed Oct 12 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.59-3
- Fix processing of input files in the NoverifyConfigs module, so configuration
  files in lists of input files are now correctly backed up inside cleanconf
  and dirtyconf directories
  Resolves: #1281452

* Thu Oct 06 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.59-2
- Remove patch leftovers

* Thu Oct 06 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.59-1
- Rebase to 0.6.59
- fix of texts in system/grub module
  Resolves: #1229341

* Wed Sep 21 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.58-1
- Rebase to 0.6.58
- fix "failed" modules because of exit_failed without log risk
- fix stray output in system/AddOns
- Resolves: #1373304 #1378096 #1378016

* Tue Sep 20 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.57-1
- Rebase to 0.6.57
- Corrects typos in texts #1253267
- Adds risk log to usrmgmt/ReservedIDs,services/openldap,
  services/freeradius,services/httpd
- Fixing usrmgmt/ReservedIDs to correctly evaluate UID/GID
- Resolves #1253267 #1376678 #1371816 #1372608

* Wed Aug 31 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.56-1
- Rebase to 0.6.56 - update stored config file of httpd (httpd.conf)
  according to current one in RHEL 6
  Resolves: #1318572

* Tue Aug 30 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.55-1
- Rebase to 0.6.55 due to error in last build

* Fri Aug 26 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.54-1
- removes whitespace causing errors
- Resolves: #1370063

* Fri Aug 12 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.53-1
- fixes httpd - previous fix was partially reverted by mistake during merging
  and diff was still reverted according to report
- fixes troubles with not enabled services after upgrade according message
  in report (result) file
- fixes issue in the module DangerousRanges to take correctly UID/GID for
  LDAP users
- Resolves: #1318572 #1365835 #1320900

* Fri Jul 29 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.52-1
- Rebase to 0.6.52
- modules do not use obsoleted functions from API (log_none_risk)
- correction of texts in various modules
- improved vsftpd module
- always require autorelabel after inplace upgrade
- backup untracked files when are included in the noverifycfg list
- fix NonRHSignedPkg to use current API and print correct results
- supress useless warning messages
- fix and improve httpd module
- added new module for open LDAP
- backup bind chroot configuration files
- Resolves: #1356057 #1253267 #1360824 #1358826 #1096885 #1281452 #1358847 #1308582
            rhbz#1315115 #1318572 #1356811 #1356808 #1358662 #1240746 #1229341 #1361327

* Wed May 04 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.49-1
- Rebase to 0.6.49
- update InterVariants, notbase-channel and LoadBalanceSupport
  Resolves: #1247738 #1296934 #1269634

* Fri Apr 29 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.48-1
- Rebase to 0.6.48
- previous tarball wasn't updated
- update SystemVersion module
  Resolves: #1247738 #1247739 #1247741 #1305487

* Thu Apr 28 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.47-1
- Rebase to 0.6.47
- modified texts in ReplacedPackages, ObsoletedPackages, notbase-channel
- use $NOAUTO_POSTUPGRADE_D in notbase-channel
  Resolves: #1247738 #1247739 #1247741

* Wed Apr 27 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.46-1
- rebase to 0.6.46
- fix dependency filter in spec - Thanks ffesti and phracek
- do not verify (md5 size mtime) of file all-xccdf.xml
- changes obsoletes of preupgrade-assistant-contents due to newer
  version built as hotfix
- requires preupgrade-assistant >= 2.1.6 because of newer API
- modify contents around package sets (ReplacedPackages, ...)
- fix regex in system/Debuginfo content
- Resolves: #1247739 #1247738 #1278675 #1247741 #1296161

* Tue Mar 1 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.6.45-1
- rebase
- pstodulk: added LICENSE file
- added content LoadBalanceSupport
  Content to check compatibility of installed Load Balancer software with RHEL 7
- Resolves: #1269634

* Mon Feb 29 2016 Petr Stodulka <pstodulka@redhat.com> - 0.6.44-1
- rebase
- fix error during upgrade selinux-target, update SystemVersion
- fix configchanges - crash de to wrong parsing of common file
- another many fixes due to new API
- Resolves: #1172547 #1295267 #1279032 #1247739 #1299218 #1305487 #1312335

* Fri Feb 26 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.43-3
- clean specfile

* Tue Feb 16 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.43-2
- added patch fo SystemVersion module

* Tue Feb 09 2016 Petr Stodulka <pstodulk@redhat.com> - 0.6.43-1
- original package was replaced by
  - preupgrade-assistant-el6toel7
  - preupgrade-assistant-el6toel7-data
- requires preupgrade-assistant >= 2.1.4-4 due to new API used in contents
- Resolves: #1296934 #1296161 #1296268 #1247739 #1247741 #1247738

* Thu Dec 03 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.41-2
- fix content system/requirements - fix check of required memory size
  Resolves: #1282275

* Fri Nov 06 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.41-1
- rebase
- fix troubles with kernel-kdump & zipl on s390x arch
- fix installation of mod_ldap in postscript (services/hhtp/)
- Resolves: rhbz#1241760, rhbz#1273867)

* Tue Oct 27 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.40-1
- rebase
- disable rhnplugins in pkgdowngrades
- fixed troubles with old server's private ssh-keys after upgrade
- fixed troubles with ZIPL
- Resolves: #1273867, #1269639, #1261500

* Wed Oct 14 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.39-1
- rebase
- no changes, just refresh of static data

* Wed Oct 07 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.38-1
- rebase
- yaboot content for ppc64 rewritten

* Thu Sep 10 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.37-1
- rebase
- modified verified_blacklist in content NoverifyConfigs - some files
  shouldn't be copied to cleanconf even when they are not dirty
  Resolves: #1261798

* Mon Sep 07 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.36-1
- rebase
- SonameRemoval fixed

* Fri Sep 04 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.35-1
- rebase
- removed checks: rsync
- added checks: java-provides
- service of contents: Soname*, BinariesRebuild, mysql, ypserv...

* Wed Aug 19 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.34-1
- rebase
- added verified_blacklist for NoverifyConfigs
- added CgroupDaemon content
- fix enforce_downgraded - now check noverifyssl option from redhat-upgrade-tool
- fix httpd postupgrade script - mod_ldap wasn't installed + some wrong regexps
  Resolves: #1241760, #1242812, #1232848, #1235356, #1253667, #1241760, #1233370

* Wed Aug 12 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.33-1
- fix yaboot and kernel-kdump
  Resolves: #1252184, #1252848, #1252186,

* Mon Aug 10 2015 Petr Stodulka <pstodulk@redhat.com> - 0.6.32-4
- rebuild due to corrupted rpm

* Mon Aug 10 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.32-3
- do not package unpatched original file

* Mon Aug 10 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.32-2
- yaboot.patch added

* Wed Jul 08 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.32-1
- rebase
- yaboot fixed, kernel-kdump fixed

* Fri Jun 19 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.30-1
- rebase
- many fixes
- new content: aide, power6
- luks allowed

* Wed May 13 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.25-1
- rebase with minor fixes only

* Mon May 11 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.24-1
- rebase with minor fixes only

* Thu May 07 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.23-2
- Rebuilt contents because of new preupgrade-assistant functionality
- New value called result_part is introduced

* Thu May 07 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.23-1
- rebase
- packages/RemovedPackages updated

* Wed Apr 22 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.22-1
- rebase
- system/repositories check script fixed

* Tue Apr 21 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.21-1
- rebase
- ifcfg check script rewritten
- SupportedVariants and SystemVersion contents merged

* Mon Mar 30 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.20-1
- rebase
- new and fixed contents: kernel-kdump, yaboot, repositories, tuned, postfix, sendmail, removable media, ...
- we are upgrading rhel-6.7->7.1 now

* Mon Feb 09 2015 Petr Hracek <phracek@redhat.com> - 0.6.18-1
- Typo in sources
- requires new preupgrade-assistant

* Mon Feb 09 2015 Petr Hracek <phracek@redhat.com> - 0.6.17-1
- --nogpg check fix and rpm._RPMVSF_NOSIGNATURES flag

* Thu Feb 05 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.1-2
- dependency filtering worked only in Fedora, now fixed for rhel6

* Thu Jan 29 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.16-1
- rebase, updated dark-matrix

* Thu Jan 29 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.15-1
- rebase, support s390x

* Fri Jan 16 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.14-2
- do not package compose_id

* Thu Jan 15 2015 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.14-1
- rebase, TaskJuggler, uefi, fixed pkgdowngrades

* Mon Dec 08 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.12-1
- rebase, vsftpd typo

* Wed Dec 03 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.11-1
- rebase, grubby workaround

* Thu Nov 20 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.10-1
- rebase

* Mon Oct 27 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.9-1
- check section instead of extra entries in files section
- changes in contents: dhcpd, system requirements, hal, openssh-keycat, rsync, rsyslog, php, vsftpd

* Fri Oct 24 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.8-1
- do not require php

* Mon Oct 20 2014 Petr Hracek <phracek@redhat.com> - 0.6.7-2
- bump version. Dark-martrix upgrade path RHEL-6.6-> RHEL7.1

* Wed Oct 08 2014 Petr Hracek <phracek@redhat.com> - 0.6.7-1
- Rebuild because of dark-matrix

* Mon Oct 06 2014 Petr Hracek <phracek@redhat.com> - 0.6.6-1
- Revert subscription-manager fix and pkgdowngrades

* Mon Oct 06 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.5-1
- version bump after pkgdowngrades fix (praiskup)

* Thu Oct 02 2014 Petr Hracek <phracek@redhat.com> - 0.6.4-1
- bump version
- missing XML files

* Thu Oct 02 2014 Petr Hracek <phracek@redhat.com> - 0.6.3-2
- system/hal content removed
- vstfpd, rsync, openssh-keycat, rsyslog  contents removed

* Wed Oct 01 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.3-1
- system/hal ready

* Wed Oct 01 2014 Petr Hracek <phracek@redhat.com> - 0.6.2-3
- Fix for enforcing SELinux (#1145147)
- Editing grub after inplace upgrade (#1146537)
- Rename postupgrade script (#1145184)
- Rsync content
- Initial PHP content

* Thu Sep 11 2014 Petr Hracek <phracek@redhat.com> - 0.6.2-2
- fix typo in pam content

* Thu Sep 11 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.2-1
- version bump

* Wed Sep 10 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.6.1-1
- pkgdowngrades content is crucial and requires redhat-upgrade-tool

* Tue Sep 09 2014 Petr Hracek <phracek@redhat.com> - 0.6.0-1
- Added Requires yum-plugin-downloadonly needed by preupgrade-scripts

* Fri Sep 05 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.17-1
- unneeded BuildRequires deleted

* Wed Aug 20 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.16-1
- version bump

* Thu Jul 31 2014 Petr Hracek <phracek@redhat.com> - 0.5.15-1
- polkit content
- created group FHS for related issues
- nfsv2 content
- fix usr.sh script for detection /usr partition

* Tue Jun 24 2014 Petr Hracek <phracek@redhat.com> - 0.5.14-1
- quorum content content
- initscripts matrix

* Fri Jun 06 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.13-1
- minor fixes

* Wed Jun 04 2014 Petr Hracek <phracek@redhat.com> - 0.5.12-1
- fix in usr content

* Wed Jun 04 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.11-1
- fix in iptables

* Fri May 30 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.10-1
- selinux/samba content

* Thu May 29 2014 Petr Hracek <phracek@redhat.com> - 0.5.9-1
- Add network/squid content
- Fix in luks content

* Wed May 28 2014 Petr Hracek <phracek@redhat.com> - 0.5.8-4
- Fix in initscripts
- Correct typo in AddOns

* Mon May 26 2014 Petr Hracek <phracek@redhat.com> - 0.5.8-3
- Content for luks crypted partitions (#1081035)

* Thu May 22 2014 Petr Hracek <phracek@redhat.com> - 0.5.8-2
- Fix for fixreplaced.sh script

* Thu May 22 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.8-1
- CleanPackages are part of Removed rpms.

* Wed May 21 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.7-1
- new contents: Gnome, KDE, QemuGuestAgent, pkgdowngrades, hyperv, optional-channel
- minor fixes

* Mon May 19 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.6-1
- spec file should be the same for contents and contents-users
- version.txt dropped, was not included into spec file anyway

* Mon May 19 2014 Petr Hracek <phracek@redhat.com> - 0.5.5-2
- Fix in HyperV content

* Wed May 14 2014 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5.5-1
- New upstream version

* Mon May 05 2014 Petr Hracek <phracek@redhat.com> - 0.5.3-3
- add 3rdparty directory

* Wed Apr 30 2014 Pavel Raiskup <praiskup@redhat.com> - 0.5.3-2
- add generated repo-stats data

* Fri Apr 25 2014 Petr Hracek <phracek@redhat.com> 0.5.3-1
- HA and Cluster content

* Fri Apr 18 2014 Petr Hracek <phracek@redhat.com> 0.5.2-1
- DTS content and some fixes

* Fri Apr 18 2014 Petr Hracek <phracek@redhat.com> 0.5.0-6
- Content for check RHEL variant

* Mon Apr 07 2014 Petr Hracek <phracek@redhat.com> 0.5.0-5
- Fix in scl-collection content

* Mon Mar 31 2014 Petr Hracek <phracek@redhat.com> 0.5.0-4
- Fix in scl-collection content

* Mon Mar 31 2014 Petr Hracek <phracek@redhat.com> 0.5.0-3
- Several fixes
- Using only group.ini files

* Wed Mar 19 2014 Petr Hracek <phracek@redhat.com> 0.5.0-2
- Several fixes
- subscription content
- initscript contents (general, rc_local, control)
- DNSMASQ content
- fixed bacula content and postupgrade script

* Wed Mar 19 2014 Petr Hracek <phracek@redhat.com> 0.5.0-1
- Fix for relative links in case of file tag
- Initscripts contents
- Usr mount content
- CVS, SVN, GIT content

* Mon Mar 17 2014 Petr Hracek <phracek@redhat.com> 0.4.5-1
- Fix for #1077157
- More PKI contents

* Mon Feb 10 2014 Petr Hracek <phracek@redhat.com> 0.4.3-2
- replace yaml with ini
- introduce bacula content

* Wed Dec 11 2013 Petr Hracek <phracek@redhat.com> 0.4.2-7
- Fixes for LC_ALL locales

* Tue Dec 10 2013 Petr Hracek <phracek@redhat.com> 0.4.2-6
- contents with verify option, mount points and filesystem change
- some fixes
- some corrections in bind content

* Mon Dec 09 2013 Petr Hracek <phracek@redhat.com> 0.4.2-5
- contents should use correct API

* Mon Dec 09 2013 Petr Hracek <phracek@redhat.com> 0.4.2-4
- In case that check_bin does not fit then return FAILED

* Wed Dec 04 2013 Petr Hracek <phracek@redhat.com> 0.4.2-3
- description field in XML does not use any HTML tag

* Fri Nov 29 2013 Petr Hracek <phracek@redhat.com> 0.4.2-2
- group_title tag in YAML files is not needed anymore

* Fri Nov 29 2013 Petr Hracek <phracek@redhat.com> 0.4.2-1
- Bump version

* Fri Nov 29 2013 Petr Hracek <phracek@redhat.com> 0.4.1-2
- Correct package description

* Thu Nov 28 2013 Petr Hracek <phracek@redhat.com> 0.4.1-1
- Initial preupgrade-assistant-contents-users rpm
